require './Hand'
require './Deck'
require './Card'

class Dealer < Hand
  attr_accessor :deck, :top_card

  def initialize
    super
    @deck = Deck.new
    @top_card = nil
  end

  def init_hand
    deal_initial_hand(@deck)
    @top_card = cards.first
  end

  def deal_card
    @deck.deal_card
  end

  def reset
    @deck.shuffle
    initialize
  end

  def hit_me
    card = deal_card
    handle_new_card(card)
  end

end
