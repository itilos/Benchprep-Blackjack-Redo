require 'test/unit'
require './Deck'
require './Hand'
require './Card'

class CardTest < Test::Unit::TestCase
  def setup
    @card = Card.new(:hearts, :ten, 10)
  end

  def test_card_suite_is_correct
    assert_equal @card.suite, :hearts
  end

  def test_card_name_is_correct
    assert_equal @card.name, :ten
  end
  def test_card_value_is_correct
    assert_equal @card.value, 10
  end

  def test_art_is_correct
    random_card_val = rand(8) + 2
    random_suite_val = rand(3)
    test_suite = Deck::SUITES[random_suite_val]
    card_name = Deck::NAME_VALUES.key(random_card_val)
    mystery = Card.new(test_suite, card_name, random_card_val)

    mystery.print_art
    puts card_name.to_s + ' of ' << test_suite.to_s

  end

end

class DeckTest < Test::Unit::TestCase
  def setup
    @deck = Deck.new
  end

  def test_new_deck_has_52_playable_cards
    assert_equal @deck.playable_cards.size, 52
  end

  def test_dealt_card_should_not_be_included_in_playable_cards
    card = @deck.deal_card
    assert(!@deck.playable_cards.include?(card))
  end

  def test_shuffled_deck_has_52_playable_cards
    @deck.shuffle
    assert_equal @deck.playable_cards.size, 52
  end
end


class HandTest < Test::Unit::TestCase
  def setup
    @deck = Deck.new
    @ghost_hand = Hand.new
  end

  def test_initial_hand_state
    @ghost_hand.deal_initial_hand(@deck)

    assert_equal(2, @ghost_hand.cards.size)

  end

  def test_initial_hand_with_one_ace
    random_card_val = rand(8) + 2
    card_name = Deck::NAME_VALUES.key(random_card_val)
    ace = Card.new(:hearts, :ace, [11, 1])
    mystery = Card.new(:hearts, card_name, random_card_val)

    cards = []

    cards << ace
    cards << mystery

    @ghost_hand.deal_initial_hand_from_card_array(cards)

    assert_equal([mystery.value + 1, mystery.value + 11], @ghost_hand.possible_card_values)

  end

  def test_initial_hand_with_two_aces
    ace = Card.new(:hearts, :ace, [11, 1])
    ace2 = Card.new(:diamonds, :ace, [11, 1])

    cards = []
    cards << ace
    cards << ace2

    @ghost_hand.deal_initial_hand_from_card_array(cards)

    assert_equal([2, 12, 12, 22], @ghost_hand.possible_card_values)

  end


end

# these comments are being saved to test in future if needed
# puts "Hand cards: "
#
# @ghost_hand.cards.each do |card|
#   puts card.name
# end
#
# puts "Possible hand values: "
#
# @ghost_hand.possible_card_values.each do |value|
#   puts value
# end
