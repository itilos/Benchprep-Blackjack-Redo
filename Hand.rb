class Hand
  attr_accessor :cards, :possible_card_values

  ACE_LOW_VALUE = 1
  ACE_HIGH_VALUE = 11


  def initialize
    @cards = []
    @possible_card_values = [0] # this is an array to handle possible ace values
    @num_aces = 0 # every ace doubles card value possibilities
  end

  # this assumes a shuffled, ready-to-go deck
  def deal_initial_hand(deck)
    card1 = deck.deal_card
    card2 = deck.deal_card

    handle_new_card(card1)
    handle_new_card(card2)

  end

  # primarily for testing
  def deal_initial_hand_from_card_array(cards)
    cards.each do |card|
      handle_new_card(card)
    end
  end

  # handles adding new cards to the hand, including ace cards
  def handle_new_card(card)
    mapped_array = Array.new(@possible_card_values)
    if card.name != :ace
      @possible_card_values.each_index do |i|
        mapped_array[i] += card.value
      end
    else
      @num_aces += 1
      new_card_value_array_size = 2 ** @num_aces

      # this essentially doubles the size of the array by copying the current elements over. there's probably a better
      # way to do this
      temp_array = Array.new(mapped_array)
      temp_array.each do |copy_value|
        mapped_array << copy_value
      end

      # this is a basic print in the UNLIKELY EVENT that something goes wrong (test show that it shouldn't)
      if mapped_array.size != new_card_value_array_size
        puts 'Card Value size is incorrect: ' << mapped_array.size.to_s
        puts 'It should be: ' << new_card_value_array_size.to_s
      end

      # now we add the new ace value to the array

      add_lower_value = true # when this is true we add 1 to a card, otherwise we add 11

      temp_array = Array.new(mapped_array)

      mapped_array.each_index do |i|
        temp_array[i] += case add_lower_value # I don't like this formatting but apparently it's 'correct style'
                         when true
                           ACE_LOW_VALUE
                         else
                           ACE_HIGH_VALUE
                         end

        if i == (mapped_array.size / 2) - 1
          add_lower_value = !add_lower_value
        end
      end

      mapped_array = Array.new(temp_array)

    end

    # now add the new card to the hand and set the new array of possible card values!
    @cards << card
    @possible_card_values = Array.new(mapped_array)

  end

  def hasBust
    @possible_card_values.each do |possible_value|
      if possible_value <= 21
        return false
      end
    end
    return true
  end

end