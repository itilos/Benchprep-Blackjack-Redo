require './Card_Art'

class Card
  attr_accessor :suite, :name, :value

  def initialize(suite, name, value)
    @suite, @name, @value = suite, name, value
  end

  def print_art
    art_loc = ART_ORDER[suite]

    case name
      when :ace
        print ACES[art_loc]
      when :two
        print TWOS[art_loc]
      when :three
        print THREES[art_loc]
      when :four
        print FOURS[art_loc]
      when :five
        print FIVES[art_loc]
      when :six
        print SIXES[art_loc]
      when :seven
        print SEVENS[art_loc]
      when :eight
        print EIGHTS[art_loc]
      when :nine
        print NINES[art_loc]
      when :ten
        print TENS[art_loc]
      when :jack
        print JACKS[art_loc]
      when :queen
        print QUEENS[art_loc]
      when :king
        print KINGS[art_loc]
    end

  end
end