require './Hand'
require './Deck'
require './Card'

class Player < Hand

  def initialize(dealer)
    super()
    @dealer = dealer
  end

  def init_hand
    deal_initial_hand(@dealer.deck)
  end

  def hit_me
    card = @dealer.deal_card
    handle_new_card(card)
  end

end
