# Main Game class (runs the Blackjack game)
require './Dealer'
require './Player'
require './Card_Art'
require 'colorize'

PLAYER_CARD_READOUT = 'Your cards are as follows:'.freeze
DEALER_CARD_READOUT = 'Dealer\'s top card is: '.freeze
DECISION_QUESTION = 'What would you like to do? You can either type \'Hit\' or \'Stand\''.freeze
BREAKER = '---------------------------------------'.freeze
ACCEPTABLE_PLAYER_MOVES = ['Stand', 'stand', 'Hit', 'hit'].freeze
ACCEPTABLE_PLAYER_END_GAME = ['Yes', 'yes', 'y', 'Y', 'No', 'no', 'N', 'n'].freeze
NEW_GAME_WHITE_SPACE = '



















'.freeze

def get_input
  res = gets
  res = res.chomp
  return res
end

def exit_game
  puts 'Thanks for playing! See ya!'
  exit(0)
end

def print_cards(hand)

  # first print the art of the card, then the name (horizontal layout would require art overhaul)
  hand.cards.each_index do |i|
    hand.cards[i].print_art
    puts ''
    print hand.cards[i].name.to_s + ' of ' + hand.cards[i].suite.to_s
    puts ''
  end

  puts ''
end

# this simulates the player turn. the dealer will respond once the player is done
def simulate_player_turn(player, dealer)
  puts PLAYER_CARD_READOUT
  print_cards(player)
  puts BREAKER
  puts DEALER_CARD_READOUT
  dealer.top_card.print_art
  puts ''
  puts dealer.top_card.name.to_s + ' of ' + dealer.top_card.suite.to_s
  puts ''
  puts DECISION_QUESTION

  response = get_input

  until ACCEPTABLE_PLAYER_MOVES.include? response
    puts 'Invalid input, please choose to \'Stand\' or \'Hit\''
    response = get_input
  end

  if response == 'Stand' || response == 'stand'
    return true
  elsif response == 'Hit' || response == 'hit'
    player.hit_me
    return false
  end

end

def game_over(winner, loser, player_bust, tie)

  if tie
    puts 'Your final hand is as follows: '
    print_cards(winner)
    puts 'Dealer\'s final hand is as follows: '
    print_cards(loser)
    puts "YOU'VE TIED! See above for final hands.".yellow
  elsif player_bust
    puts 'Your final hand is as follows: '
    print_cards(loser)
    puts 'Dealer\'s final hand is as follows: '
    print_cards(winner)
    puts "YOU'VE BUST! DEALER WINS! See above for final hands.".red
  else
    class_check = winner.class
    if class_check == Player
      puts 'Your final hand is as follows: '
      print_cards(winner)
      puts 'Dealer\'s final hand is as follows: '
      print_cards(loser)
      puts "YOU WIN! See above for final hands.".green
    elsif class_check == Dealer
      puts 'Your final hand is as follows: '
      print_cards(loser)
      puts 'Dealer\'s final hand is as follows: '
      print_cards(winner)
      puts "DEALER WINS! See above for final hands.".blue
    end

  end

  puts 'Would you like to play again? Answer \'Yes\' for another round or \'No\' to exit!'
  response = get_input
  until ACCEPTABLE_PLAYER_END_GAME.include? response
    puts 'Invalid input, please choose to \'Yes\' or \'No\''
    response = get_input
  end

  if response == 'No' || response == 'no' || response == 'n' || response == 'N'
    exit_game
  else
    play_game
  end

end

def simulate_dealer_turn(dealer)
  dealer_must_stop = false
  dealer.possible_card_values.each do |value|
    if value >= 17
      dealer_must_stop = true
      break
    end
  end

  until dealer_must_stop
    dealer.hit_me
    dealer.possible_card_values.each do |value|
      if value >= 17
        dealer_must_stop = true
        break
      end
    end
  end

end

def play_game
  puts NEW_GAME_WHITE_SPACE
  puts 'Beginning a new round! Dealing cards...'

  dealer = Dealer.new
  player = Player.new(dealer)
  user_response = ''

  player_done = false

  loop do
    player.init_hand
    dealer.init_hand

    while !player.hasBust && !player_done
      player_done = simulate_player_turn(player, dealer)
    end

    if player.hasBust
      game_over(dealer, player, true, false)
    end

    simulate_dealer_turn(dealer)

    if dealer.hasBust
      game_over(player, dealer, false, false)
    end

    # if neither the player nor dealer has bust
    player_max_value = 0
    dealer_max_value = 0

    player.possible_card_values.each do |value|
      if value > player_max_value && value <= 21
        player_max_value = value
      end
    end

    dealer.possible_card_values.each do |value|
      if value > dealer_max_value && value <= 21
        dealer_max_value = value
      end
    end

    if player_max_value > dealer_max_value
      game_over(player, dealer, false, false)
    elsif player_max_value < dealer_max_value
      game_over(dealer, player ,false, false)
    else
      game_over(player, dealer, false, true)
    end

  end

end

if __FILE__ == $PROGRAM_NAME
  puts 'Welcome to Joe\'s mystical Blackjack!'
  puts 'Type \'Play\' to play, or \'Exit\' to quit!'

  response = get_input

  if response == 'Play' || response == 'play'
    play_game
  elsif response == 'Exit' || response == 'exit'
    exit_game
  end

end



